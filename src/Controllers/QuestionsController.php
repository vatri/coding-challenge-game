<?php


namespace Ucc\Controllers;


use Ucc\Http\JsonResponseTrait;
use Ucc\Services\QuestionService;

class QuestionsController extends Controller
{
    use JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        $this->questionService->beginGame($name);

        $question = $this->questionService->getCurrentQuestion();

        return $this->json(['question' => $question], 201);
    }

    public function answerQuestion(int $id): bool
    {
        if ( $this->questionService->getPlayerName() === null ) {
            return $this->json('You must first begin a game', 400);
        }

        if ($this->questionService->isAllAnswered()) {
            $name = $this->questionService->getPlayerName();
            $points = $this->questionService->getPlayerScore();
            $this->questionService->finishGame();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json(['message' => 'You must provide an answer'], 400);
        }

        $points = $this->questionService->answerQuestion($id, $answer);

        $message = $points > 0 ? 'Correct answer' : "Wrong answer";

        $question = $this->questionService->getCurrentQuestion();

        return $this->json([
            'message' => $message,
            'question' => $question,
            'currentScore' => $this->questionService->getPlayerScore()
        ]);
    }
}