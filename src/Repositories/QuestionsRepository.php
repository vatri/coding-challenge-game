<?php


namespace Ucc\Repositories;


use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionsRepository
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private array $questions;

    public function __construct(JSON $json, \JsonMapper $jsonMapper)
    {
        $questions = $json->decode(file_get_contents(self::QUESTIONS_PATH));
        $this->questions = $jsonMapper->mapArray($questions, array(), Question::class);
    }

    public function findAll(): array
    {
        return $this->questions;
    }

    public function findById(int $id): ?Question
    {
        foreach ($this->questions as $question) {
            if ($question->getId() == $id) {
                return $question;
            }
        }
        return null;
    }



}