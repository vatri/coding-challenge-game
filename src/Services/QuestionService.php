<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;
use Ucc\Repositories\QuestionsRepository;
use Ucc\Session;

class QuestionService
{
    const KEY_CURRENT_QUESTION = 'currentQuestion';
    const KEY_QUESTION_POINT = 'points';
    const KEY_PLAYER_NAME = 'playerName';

    private SessionService $sessionService;
    private QuestionsRepository $questionsRepository;
    private array $questions;

    public function __construct(SessionService $sessionService, QuestionsRepository $questionsRepository)
    {
        $this->questionsRepository = $questionsRepository;
        $this->sessionService = $sessionService;
        $this->questions = $questionsRepository->findAll();
    }

    /**
     * @param string $name Player name
     */
    public function beginGame(string $name): void
    {
        $this->sessionService->set(self::KEY_PLAYER_NAME, $name);
        $this->sessionService->set(self::KEY_CURRENT_QUESTION, 1);
        $this->sessionService->set(self::KEY_QUESTION_POINT, 0);
    }

    public function finishGame(): void
    {
        $this->sessionService->destroy();
    }

    public function getCurrentQuestion(): ?Question
    {
        $currentIndex = $this->sessionService->get(self::KEY_CURRENT_QUESTION) - 1;
        if ($currentIndex > $this->getTotalQuestionsNumber()-1) {
            return null;
        }
        return $this->questions[$currentIndex];
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $question = $this->questionsRepository->findById($id);
        if (!$question) {
            throw new \Exception("Wrong ID provided");
        }

        return $question->getCorrectAnswer() == $answer ? $question->getPoints() : 0;
    }

    public function getTotalQuestionsNumber()
    {
        return count($this->questions);
    }

    /**
     * @param int $id Question ID
     * @param string $answer
     * @return int Number of point gathered for the answer
     */
    public function answerQuestion(int $id, string $answer): int
    {
        $points = $this->getPointsForAnswer($id, $answer);

        $currentQuestion = $this->getCurrentQuestion();

        // Increment session counting immediately to prevent asking one question more than once:
        $this->setNextQuestion();

        // Make sure we can only reply next question:
        if ($id != $currentQuestion->getId()) {
            return 0;
        }

        $this->incrementPlayerPoints($points);

        return $points;
    }

    /**
     * @param $points
     */
    private function incrementPlayerPoints(int $points): void
    {
        $total_points = $this->sessionService->get(self::KEY_QUESTION_POINT);
        $total_points += $points;
        $this->sessionService->set(self::KEY_QUESTION_POINT, $total_points);
    }

    private function setNextQuestion(): void
    {
        $questions_count = Session::get(self::KEY_CURRENT_QUESTION);
        $questions_count++;
        $this->sessionService->set(self::KEY_CURRENT_QUESTION, $questions_count);
    }

    public function isAllAnswered(): bool
    {
        return (int)$this->sessionService->get(self::KEY_CURRENT_QUESTION) >= $this->getTotalQuestionsNumber();
    }

    public function getPlayerName(): ?string
    {
        return $this->sessionService->get(self::KEY_PLAYER_NAME);
    }

    public function getPlayerScore(): ?int
    {
        return $this->sessionService->get(self::KEY_QUESTION_POINT);
    }


}