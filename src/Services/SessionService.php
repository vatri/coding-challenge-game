<?php


namespace Ucc\Services;


use Ucc\Session;

class SessionService  implements SessionServiceInterface
{
    public function start()
    {
        Session::start();
    }

    public function destroy()
    {
        Session::destroy();
    }

    public function set(string $key, string $value)
    {
        Session::set($key, $value);
    }

    public function get(string $key): ?string
    {
        return Session::get($key);

    }
}