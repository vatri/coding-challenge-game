<?php
namespace Ucc\Services;

interface SessionServiceInterface
{
    public function start();

    public function destroy();

    public function set(string $key, string $value);

    public function get(string $key): ?string;
}