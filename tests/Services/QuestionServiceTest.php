<?php

namespace Tests;

use KHerGe\JSON\JSON;
use Ucc\Models\Question;
use Ucc\Repositories\QuestionsRepository;
use Ucc\Services\QuestionService;
use PHPUnit\Framework\TestCase;
use Ucc\Services\SessionService;

class QuestionServiceTest extends TestCase
{
    protected QuestionService $questionService;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testAnswers()
    {
        $mockQuestionsRepository = $this->getMockBuilder(QuestionsRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $points = 5;

        $question = new Question();
        $question->setId(1);
        $question->setCorrectAnswer('correct answer');
        $question->setPoints($points);

        $mockQuestionsRepository->method('findAll')->willReturn([
           $question
        ]);
        $mockQuestionsRepository->method('findById')->willReturn($question);

        $mockSessionService = $this->getMockBuilder(SessionService::class)
            ->getMock();
        $questionService = new QuestionService($mockSessionService, $mockQuestionsRepository);

        // Test correct answer
        $this->assertEquals($points, $questionService->getPointsForAnswer(1, 'correct answer'));

        // Test wrong answer
        $this->assertEquals(0, $questionService->getPointsForAnswer(1, 'wrong answer'));
    }

}
